# SHSUCDX

Free CDROM extender for DOS


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## SHSUCDX.LSM

<table>
<tr><td>title</td><td>SHSUCDX</td></tr>
<tr><td>version</td><td>3.08a</td></tr>
<tr><td>entered&nbsp;date</td><td>2021-02-17</td></tr>
<tr><td>description</td><td>Free CDROM extender for DOS</td></tr>
<tr><td>keywords</td><td>mscdex, shsucdex, fdcdex, cdrom, atapicdd, xcdrom</td></tr>
<tr><td>author</td><td>John H. McCoy</td></tr>
<tr><td>maintained&nbsp;by</td><td>Jason Hood &lt;jadoxa -at- yahoo.com.au&gt;</td></tr>
<tr><td>primary&nbsp;site</td><td>http://adoxa.altervista.org/shsucdx/index.html</td></tr>
<tr><td>alternate&nbsp;site</td><td>https://github.com/adoxa/shsucd</td></tr>
<tr><td>mirror&nbsp;site</td><td>http://www.ibiblio.org/pub/micro/pc-stuff/freedos/files/dos/shsucdx/</td></tr>
<tr><td>platforms</td><td>DOS (NASM), FreeDOS</td></tr>
</table>
